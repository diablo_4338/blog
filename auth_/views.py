from django.contrib.auth.views import LoginView, LogoutView


# Create your views here.
class CustomLogin(LoginView):
    template_name = 'auth_/login.html'


class CustomLogout(LogoutView):
    template_name = None
    next_page = 'index'

