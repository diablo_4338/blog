"""blog URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path
from .views import Index, Subscribe, PostDetail, ReadPost
from django.contrib.auth.decorators import login_required

urlpatterns = [
    path('', Index.as_view(), name='index'),
    path('subscribe/', login_required(Subscribe.as_view(), login_url='login', redirect_field_name=None),
         name='subscribe'),
    path('<int:pk>/', login_required(PostDetail.as_view(), login_url='login', redirect_field_name=None),
         name='post_detail'),
    path('readpost/', login_required(ReadPost.as_view(), login_url='login', redirect_field_name=None),
         name='read_post'),

]
