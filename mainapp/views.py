from django.shortcuts import render
from django.views.generic import TemplateView
from django.views.generic.edit import FormView
from .models import Blog, Post
from django import forms
from django.shortcuts import redirect
from django.db.models import Q


# Create your views here.

class SubscribeForm(forms.Form):
    action = forms.CharField()
    targed_blog = forms.IntegerField()
    redirect_to = forms.CharField()


class ReadPostForm(forms.Form):
    post_id = forms.IntegerField()
    redirect_to = forms.CharField()


class Subscribe(FormView):
    form_class = SubscribeForm

    def form_valid(self, form):
        original_blog = Blog.objects.get(author=self.request.user)
        targed_blog = Blog.objects.get(id=self.request.POST.get('targed_blog'))
        if targed_blog.author not in original_blog.subscribe_to.all() and self.request.POST.get(
                'action') == 'subscribe':
            original_blog.subscribe_to.add(targed_blog.author)
            original_blog.save()
        if targed_blog.author in original_blog.subscribe_to.all() and self.request.POST.get('action') == 'unsubscribe':
            original_blog.subscribe_to.remove(targed_blog.author)
            original_blog.save()
            # Удалим метки прочитанности
            posts = Post.objects.filter(blog=targed_blog).all()
            for post in posts:
                post.readed.remove(self.request.user)
                post.save()
        self.success_url = self.request.POST.get('redirect_to')
        return super(Subscribe, self).form_valid(form)

    def form_invalid(self, form):
        return redirect('index')

    def get(self, request, *args, **kwargs):
        return redirect('index')


class ReadPost(FormView):
    form_class = ReadPostForm

    def form_valid(self, form):
        post = Post.objects.get(id=self.request.POST.get('post_id'))
        post.readed.add(self.request.user)
        post.save()
        self.success_url = self.request.POST.get('redirect_to')
        return super(ReadPost, self).form_valid(form)

    def form_invalid(self, form):
        return redirect('index')

    def get(self, request, *args, **kwargs):
        return redirect('index')


def get_blogs_context(request):
    blog = Blog.objects.get(author=request.user)
    blogs = Blog.objects.all().exclude(id=blog.id)
    context = {'blogs': blogs, 'subscribe_to': blog.subscribe_to.all()}

    return context


class PostDetail(TemplateView):
    template_name = 'mainapp/post.html'

    def get_context_data(self, **kwargs):
        # TODO Получать через get_404
        context = {'post': Post.objects.get(id=kwargs.get('pk'))}
        context.update(get_blogs_context(self.request))
        return context


class Index(TemplateView):
    template_name = 'mainapp/index.html'

    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            blog = Blog.objects.get(author=request.user)
            # Получаем посты из блога пользователя и всех подписанных
            posts = Post.objects.filter(
                Q(blog=blog) | Q(blog__in=Blog.objects.filter(author__in=blog.subscribe_to.all()))).order_by(
                '-created_datetime')

            context = {'posts': posts}
            kwargs.update(context)
            kwargs.update(get_blogs_context(request))
        return super(Index, self).get(request, *args, **kwargs)
