from django.db import models
from django.contrib.auth import get_user_model
from django.utils import timezone


# Create your models here.
class Blog(models.Model):
    title = models.CharField(max_length=50, null=True, default='Блог')
    author = models.OneToOneField(get_user_model(), on_delete=models.CASCADE)
    subscribe_to = models.ManyToManyField(get_user_model(), related_name='user_blog', blank=True)

    def save(self, *args, **kwargs):
        # TODO Добавить проверку, чтобы не подписывались сами на себя
        # TODO Снимать метку прочитанности постов при отписке от блога
        super(Blog, self).save(*args, **kwargs)


class Post(models.Model):
    text = models.TextField()
    title = models.CharField(max_length=50, null=True)
    blog = models.ForeignKey(Blog, on_delete=models.CASCADE)
    created_datetime = models.DateTimeField(default=timezone.now)
    readed = models.ManyToManyField(get_user_model(), related_name='user_post', blank=True)

    def save(self, *args, **kwargs):
        # TODO рассылать email всем подписанным на блог пользователям
        super(Post, self).save(*args, **kwargs)

    # TODO Добавить метод, который возвращает URL поста
